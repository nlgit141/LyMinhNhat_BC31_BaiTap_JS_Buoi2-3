/**Bài1: TÍNH TIỀN LƯƠNG NHÂN VIÊN
* 
* *Input: 
* - ương 1 ngày : 100.000đ ;
* - số ngày làm việc người dùng tự cho.
* 
* 
* các bước xử lý: 
* - tạo biến lưu giá trị lương 1 ngày và số ngày làm;
* - tạo hàm sự kiện khai báo kết quả tiền lương;
* - áp dụng công thức: tổng lương = lương 1 ngày * số ngày làm;
* -  
*
* 
* ?Output: 
* - tổng lương = ? 
*/
var luong1Ngay = 100000;
var soNgayLamValue = document.getElementById("so-ngay-lam").value * 1;
var tongLuongValue = null;

function tinhLuong() {
    luong1Ngay = 100000;
    soNgayLamValue = document.getElementById("so-ngay-lam").value * 1;
    tongLuongValue = luong1Ngay * soNgayLamValue;
    console.log("Tổng tiền lương:", Intl.NumberFormat().format(tongLuongValue) + "VNĐ");
    document.getElementById("so-tien").innerHTML = `<div>Tổng tiền lương: ${Intl.NumberFormat().format(tongLuongValue) + "VNĐ"}</div>`;
}

// ===================================****!!!****==================================///

/**Bài 2: TÍNH GIÁ TRỊ TRUNG BÌNH
 * *Input:
 * - cho người dùng nhập 5 số tùy chọn
 * 
 * 
 * 
 * Các bước xử lý: 
 * - tạo biến lưu giá trị của 5 số thực
 * - tạo biến tính trung bình của 5 số thực
 * - áp dụng công thức: (num1 + num2 +num3 + num4 + num5) / 5
 * - tạo biến lưu kết quả



 *?Output:
 * trung bình 5 số = ?
 */
function tinhTrungBinh() {
    var num1Value = document.getElementById("txt-number1").value * 1;
    var num2Value = document.getElementById("txt-number2").value * 1;
    var num3Value = document.getElementById("txt-number3").value * 1;
    var num4Value = document.getElementById("txt-number4").value * 1;
    var num5Value = document.getElementById("txt-number5").value * 1;
    var result = (num1Value + num2Value + num3Value + num4Value + num5Value) / 5;
    console.log("num1Value:", num1Value);
    console.log("num2Value:", num2Value);
    console.log("num3Value:", num3Value);
    console.log("num4Value:", num4Value);
    console.log("num5Value:", num5Value);
    console.log("result:", result);
    document.getElementById("ketqua-trungbinh").innerHTML = `<div> Kết quả trung bình: ${result}</div>`;
}

// ===================================****!!!****==================================///

/**BÀI 3: QUY ĐỔI TIỀN 
 * *Input: 
 * - giá $1 hiện tại : 23,500 vnđ
 * 
 * - Các bước xử lý: 
 * - tạo biến lưu giá trị
 * - tạo biến lưu kết quả
 * - áp dụng công thức: USD * VNĐ
 * 
 * ?Output: 
 * - số tiền sau khi quy đổi VNĐ = ?
 */


function doiTien() {
    var giaVnd = 23500;
    var soTienQuyDoiValue = document.getElementById("money").value * 1;
    var doiTienValue = soTienQuyDoiValue * giaVnd;
    console.log("doiTien:", Intl.NumberFormat().format(doiTienValue) + "VNĐ");
    document.getElementById("quy-doi-tien").innerHTML = `<div>Tỉ giá: ${Intl.NumberFormat().format(doiTienValue) + "VNĐ"}</div>`;
}

// ===================================****!!!****==================================///

/**BÀI 4: TÍNH DIỆN TÍCH & CHU VI CHỮ NHẬT
 * *INPUT:
 * - chiều dài 
 * - chiều rộng
 * 
 * 
 * CÁC BƯỚC XỬ LÝ:
 * - tạo hàm sự kiện
 * - tạo biến lưu giá trị chiều dài và chiều rộng
 * - áp dụng công thức tính diện tích:  diện tích = chiều dài * chiều rộng
 * - áp dụng công thức tính chu vi:  chu vi = (chiều dài + chiều rộng) * 2;
 * - tạo biến lưu kết quả
 * 
 * 
 * ? OUTPUT:
 * - diện tích hình chữ nhật = ?
 *- chu vi hình chữ nhật bằng = ?
 */


function tinhDienTich() {
    var chieuRong = document.getElementById("chieu-rong").value * 1;
    var chieuDai = document.getElementById("chieu-dai").value * 1;
    console.log("chiều rộng:", chieuRong);
    console.log("chiều dài:", chieuDai);
    var dienTich = (chieuRong * chieuDai);
    console.log("diện tích:", dienTich);
    document.getElementById("ketqua-chunhat").innerHTML = `<div>Diện tích là: ${dienTich}</div>`;
}
function tinhChuVi() {
    var chieuRong = document.getElementById("chieu-rong").value * 1;
    var chieuDai = document.getElementById("chieu-dai").value * 1;
    console.log("chiều rộng:", chieuRong);
    console.log("chiều Dài:", chieuDai);
    var chuVi = (chieuRong + chieuDai) * 2;
    console.log("chu vi:", chuVi);
    document.getElementById("ketqua-chunhat").innerHTML = `<div>Chu vi là: ${chuVi}</div>`;
    document.getElementById("ketqua-chunhat").style.marginLeft = "85px"
}

// ===================================****!!!****==================================///

/**BÀI 5: TÍNH TỔNG HAI KÝ SỐ
 * *INPUT:
 * - 1 số có 2 chữ số
 * - tính tổng 2 ký số
 * 
 * 
 * các bước xử lý:
 * - tạo biến n,unit,ten,sum
 * - gán giá trị cho n;
 * - tách số hàng chục theo công thức ten = Math.floor(n%100/10)
 * - tách số hàng đơn vị theo công thức unit = n%10
 * - áp dụng công thức Sum = ten + unit;
 * - tạo biến kết quả
 * 
 * 
 * ? OUTPUT:
 * - tổng hai ký số cộng lại = ?
 */

function tinhTong_haiKySo() {
    var nValue = document.getElementById("tong-2-ky-so").value * 1;
    var nTen = Math.floor(nValue / 10);
    var nUnit = nValue % 10;
    sum = nTen + nUnit;
    console.log("2 ký số:", nValue);
    console.log("tổng 2 ký số:", sum);
    document.getElementById("tong-hai-ky-so").innerHTML = `<div> Tổng 2 ký số cộng lại là: ${sum}</div>`
}
